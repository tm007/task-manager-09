package ru.tsc.apozdnov.tm.api;

public interface ICommandController {

    void showWelcome();

    void showFaultArgument(String fault);

    void showFaultCommnand(String fault);

    void showAbout();

    void showSystemInfo();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

}
