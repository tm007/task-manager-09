package ru.tsc.apozdnov.tm.controller;

import ru.tsc.apozdnov.tm.api.ICommandController;
import ru.tsc.apozdnov.tm.api.ICommandService;
import ru.tsc.apozdnov.tm.model.Command;
import ru.tsc.apozdnov.tm.util.ConvertByteUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println("**** Welcome to Task Manager ****");
    }

    @Override
    public void showFaultArgument(String fault) {
        System.err.printf("Fault... This argument `%s` not supported...  \n", fault);
    }

    @Override
    public void showFaultCommnand(String fault) {
        System.err.printf("Fault... This command `%s` not supported...  \n", fault);
    }

    @Override
    public void showAbout() {
        System.out.println("[About]");
        System.out.println("[Name: Aleksandr Pozdnov]");
        System.out.println("[E-mail: apozdnov@t1.com]");
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableprocessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableprocessors);
        final long freememory = runtime.freeMemory();
        final String freeMemoryFormat = ConvertByteUtil.formatBytes(freememory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = Long.toString(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormatValue = ConvertByteUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (isMemoryLimit ? "no limit" : maxMemoryFormatValue);
        System.out.println("Maximum memory : " + maxMemoryFormat);
        final long totatMemory = runtime.totalMemory();
        final String totalMemoryFormat = ConvertByteUtil.formatBytes(totatMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totatMemory - freememory;
        final String usedMemoryFormat = ConvertByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    @Override
    public void showVersion() {
        System.out.println("1.9.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommand();
        for (final Command command : commands) System.out.println(command);
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommand();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommand();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
