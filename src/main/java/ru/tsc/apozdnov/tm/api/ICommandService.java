package ru.tsc.apozdnov.tm.api;

import ru.tsc.apozdnov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommand();

}
