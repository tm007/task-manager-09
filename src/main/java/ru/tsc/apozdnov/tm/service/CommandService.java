package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.ICommandRepository;
import ru.tsc.apozdnov.tm.api.ICommandService;
import ru.tsc.apozdnov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommand() {
        return commandRepository.getCommands();
    }

}
