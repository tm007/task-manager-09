package ru.tsc.apozdnov.tm.api;

import ru.tsc.apozdnov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
