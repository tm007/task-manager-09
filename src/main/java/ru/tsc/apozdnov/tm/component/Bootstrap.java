package ru.tsc.apozdnov.tm.component;

import ru.tsc.apozdnov.tm.api.ICommandController;
import ru.tsc.apozdnov.tm.api.ICommandRepository;
import ru.tsc.apozdnov.tm.api.ICommandService;
import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.controller.CommandController;
import ru.tsc.apozdnov.tm.repository.CommandRepository;
import ru.tsc.apozdnov.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String cmd = scanner.nextLine();
            processCommandTask(cmd);
        }
    }

    public void processCommandTask(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConstant.EXIT:
                close();
                break;
            default:
                commandController.showFaultCommnand(command);
                break;
        }
    }

    public void processArgumentTask(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showFaultArgument(arg);
                break;
        }
    }

    public boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

    public static void close() {
        System.exit(0);
    }

}
