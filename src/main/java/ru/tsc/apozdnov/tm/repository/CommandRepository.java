package ru.tsc.apozdnov.tm.repository;

import ru.tsc.apozdnov.tm.api.ICommandRepository;
import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConstant.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show terminal commands."
    );

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show system info."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show list arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show list commands."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application Task Manager."
    );

    private static Command[] terminalCommands = new Command[]{
            ABOUT,
            VERSION,
            HELP,
            INFO,
            ARGUMENTS,
            COMMANDS,
            EXIT
    };

    @Override
    public Command[] getCommands() {
        return terminalCommands;
    }
}
